import { ChangeDetectionStrategy, Component, OnInit, OnDestroy } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { FontFormat } from '../font-formats.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent implements OnInit, OnDestroy {

  fontFormat: FontFormat;
  subscription: Subscription;

  constructor(private textService: TextService) { }

  ngOnInit() {
    this.subscription = this.textService.fontFormatChanged.subscribe(
      (fontFormat: FontFormat) => {
        this.fontFormat = fontFormat;
        console.log(this.fontFormat);
      }
    );
    this.fontFormat = this.textService.getFontFormat();
  }

  toggleBold() {
    this.textService.changeFormat('bold', false, null);
  }

  toggleItalic() {
    this.textService.changeFormat('italic', false, null);
  }

  toggleUnderline() {
    this.textService.changeFormat('underline', false, null);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
