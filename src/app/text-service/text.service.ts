import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CustomSelection } from '../custom-selection.model';
import { FontFormat } from '../font-formats.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class TextService {

  selections: CustomSelection[] = [];
  fontFormat = new FontFormat(false, false, false);
  synonyms: string[] = [];
  selectionsChanged = new Subject<CustomSelection[]>();
  fontFormatChanged = new Subject<FontFormat>();
  synonymsChanged = new Subject<string[]>();
  currentSelection: any;

  constructor(private http: HttpClient) {}

  getMockText() {
    return new Promise<string>((resolve) => {
      resolve('A year ago I was in the audience at a gathering of designers in San Francisco. ' +
        'There were four designers on stage, and two of them worked for me. I was there to support them. ' +
        'The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. ' +
        'What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, ' +
        'that modern design problems were very complex. And we ought to need a license to solve them.');
    });
  }

  getFontFormat() {
    return this.fontFormat;
  }

  changeFormat(comandId, showUI, value) {
    document.designMode = 'on';
    document.execCommand(comandId, showUI, value);
    document.designMode = 'off';
    this.toggleFontFormat(comandId);
  }

  addSelection(selection: CustomSelection) {
    this.currentSelection = this.storageSelection();
    const selectionExist = this.selections.find(
      element =>
        element.wordLookup === selection.wordLookup &&
        element.selection === selection.selection
    );
    if (selectionExist) {
      this.fontFormat = selectionExist.fontFormat;
      this.fetchSynonyms(selectionExist.wordLookup);
    } else {
      this.selections.push(selection);
      this.selectionsChanged.next(this.selections.slice());
      this.fetchSynonyms(selection.wordLookup);
    }
  }

  toggleFontFormat(comandId) {
    switch (comandId) {
      case 'bold':
        this.fontFormat.bold = !this.fontFormat.bold;
        break;
      case 'italic':
        this.fontFormat.italic = !this.fontFormat.italic;
        break;
      case 'underline':
        this.fontFormat.underline = !this.fontFormat.underline;
        break;
      default:
        return;
    }
    this.fontFormatChanged.next(this.fontFormat);
  }

  getSynonyms() {
    return this.synonyms.slice();
  }

  fetchSynonyms(wordLookup: string) {
    return this.http.get(`${ environment.synonymsApiEndpoint }?rel_syn=${wordLookup}`)
    .pipe(
      map((results: Array<{word: string, score: number}>) => {
        return results.map(result => {
          return result.word;
        });
      }),
    )
    .subscribe(response => {
      this.synonyms = response;
      this.synonymsChanged.next(this.synonyms.slice());
    });
  }

  handleChosenSuggestion(suggestedWord) {
    const range = this.currentSelection;
    range.deleteContents();
    range.insertNode(document.createTextNode(suggestedWord));
  }

  storageSelection() {
    if (window.getSelection) {
        const sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            return sel.getRangeAt(0);
        }
    }
    return null;
  }

}
