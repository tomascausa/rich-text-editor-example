export class FontFormat {
    constructor(
        public bold: boolean,
        public italic: boolean,
        public underline: boolean
    ) {}
}
