import { FontFormat } from './font-formats.model';

export class CustomSelection {
    constructor(
        public selection: any,
        public wordLookup: string,
        public selectionPosition: DOMRect,
        public fontFormat: FontFormat
    ) {}
}
