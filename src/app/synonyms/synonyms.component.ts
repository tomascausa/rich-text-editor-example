import { Component, OnInit, OnDestroy } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-synonyms',
  templateUrl: './synonyms.component.html',
  styleUrls: ['./synonyms.component.scss']
})
export class SynonymsComponent implements OnInit, OnDestroy {

  synonyms: string[] = [];
  subscription: Subscription;

  constructor(private textService: TextService) { }

  ngOnInit(): void {
    this.subscription = this.textService.synonymsChanged.subscribe(
      (synonyms: string[]) => {
        this.synonyms = synonyms;
      }
    );
    this.synonyms = this.textService.getSynonyms();
  }

  onWordSelected(suggestionWord){
    this.textService.handleChosenSuggestion(suggestionWord);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
