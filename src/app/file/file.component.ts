import { ChangeDetectionStrategy, Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { CustomSelection } from '../custom-selection.model';
import { FontFormat } from '../font-formats.model';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileComponent implements OnInit {

  text$: Promise<string>;
  @ViewChild('text') text: ElementRef;

  constructor(private textService: TextService) {
  }

  ngOnInit() {
    this.text$ = this.textService.getMockText();
  }

  selectionHandler() {
    const windowSelection = window.getSelection();
    if (windowSelection.type === 'Range') {
      const newSelection = new CustomSelection(
        windowSelection,
        windowSelection.toString(),
        windowSelection.getRangeAt(0).getBoundingClientRect(),
        new FontFormat(false, false, false)
      );
      this.textService.addSelection(newSelection);
    }
  }

}
