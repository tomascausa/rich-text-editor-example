export const environment = {
  production: true,
  synonymsApiEndpoint: 'https://api.datamuse.com/words'
};
